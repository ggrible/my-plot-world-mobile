module.controller('MainMenuController', function($scope, $http, $rootScope){ 
    $rootScope.$on('get_user_info', function(event, userInfo){
        $scope.userInfo = userInfo;
    });
    
    $scope.logout = function() {
        showModal();
        
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
        
        $http({
            method: 'GET',
            url: BASEURL + '/auth/logout'
        }).then(function successCallback(response) {
            hideModal();
            accessToken = '';
            ons.notification.alert(response.data);
            fn.load('login.html');
        },function errorCallback(response) {
            hideModal();
            ons.notification.alert(response.data);
        });
    }
    
    $scope.goTo = function(page) {
        var menu = document.getElementById('menu');
        plotNavigator.pushPage(page).then(menu.close.bind(menu));
    }
});

module.controller('LoginController', function($scope, $http, $rootScope) {
    this.email = '';
    this.password = '';
    
    $rootScope.$on("callLoginMethod", function(event, data){
        $scope.login(data.email, data.password);
    });
  
    $scope.login = function(email, password) {
        $scope.email = email != undefined ? email : $scope.login.email;
        $scope.password = password != undefined ? password : $scope.login.password;
        
        if ( $scope.email == undefined ) {
            ons.notification.alert('Please enter email address');
            return;
        }
        
        if ( $scope.password == undefined ) {
            ons.notification.alert('Please enter password');
            return;
        }
        
        showModal();

        $http({
            method: 'POST',
            url: BASEURL + '/auth/login',
            data: {
                'email' : $scope.email,
                'password' : $scope.password
            },
            header: {
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(function successCallback(response) {
            hideModal();
            if ( response.data != '') {
                accessToken = response.data.access_token;
                $scope.getUserInfo();
            }
        },function errorCallback(response) {
            hideModal();
            ons.notification.alert(response.data);
        });
    };
    
    $scope.getUserInfo = function() {    
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
        
        $http({
            method: 'GET',
            url: BASEURL + '/auth/user'
        }).then(function successCallback(response) {
            if ( response.data != '') {
                userInfo = response.data;
                
                if ( userInfo.status == 'expired' ) {
                    plotNavigator.pushPage('activation.html');
                } else {
                    $rootScope.$broadcast("get_user_info", response.data);
                    plotNavigator.pushPage('dashboard.html', {
                        "animation": "slide"
                    });
                }
            }
        },function errorCallback(response) {
            ons.notification.alert(response.data);
        });
    }
});

module.controller('SignupController', function($scope, $http, $rootScope) {
    this.init = function()
    {
        
    };
    
    $scope.signup = function()
    {
        
        if ( $scope.signup.username == undefined ) {
            ons.notification.alert('Please enter username');
            return;
        }
        if ( $scope.signup.first_name == undefined ) {
            ons.notification.alert('Please enter first name');
            return;
        }
        if ( $scope.signup.last_name == undefined ) {
            ons.notification.alert('Please enter last name');
            return;
        }
        if ( $scope.signup.email == undefined ) {
            ons.notification.alert('Please enter email address');
            return;
        }
        if ( $scope.signup.password == undefined ) {
            ons.notification.alert('Please enter password');
            return;
        }
        if ( $scope.signup.password_confirmation == undefined ) {
            ons.notification.alert('Please enter confirm password');
            return;
        }
        if ( $scope.signup.password_confirmation != $scope.signup.password ) {
            ons.notification.alert('Password must match Confirm Password.');
            return;
        }
        
        showModal();
         
        $http({
            method: 'POST',
            url: BASEURL + '/auth/signup',
            data: {
                'name': $scope.signup.username,
                'first_name' : $scope.signup.first_name,
                'last_name': $scope.signup.last_name,
                'email' : $scope.signup.email,
                'password' : $scope.signup.password,
                'password_confirmation': $scope.signup.password_confirmation
            },
            header: {
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(function successCallback(response) {
            hideModal();
            ons.notification.alert(response.data);
            $rootScope.$broadcast("callLoginMethod", { email: $scope.signup.email, password: $scope.signup.password });
        },function errorCallback(response) {
            hideModal();
            ons.notification.alert(response.data);
        });
    };
});

module.controller('ForgotPasswordController', function($scope, $http, $rootScope) {
    
    $scope.submit = function()
    {
        $scope.email = $scope.forgotpassword.email;
        
        if ( $scope.email == undefined ) {
            ons.notification.alert('Please enter email address');
            return;
        }
        
        showModal();

        $http({
            method: 'POST',
            url: BASEURL + '/auth/reset',
            data: {
                'email' : $scope.email
            },
            header: {
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(function successCallback(response) {
            hideModal();
            ons.notification.alert('We have e-mailed your password reset link!');
        },function errorCallback(response) {
            hideModal();
            ons.notification.alert(response.data);
        });
    };
});

module.controller('DashboardController', function($scope, $http, $rootScope) {
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
    
    this.init = function(e)
    {  
        showModal();
        
        $http({
            method: 'GET',
            url: BASEURL + '/auth/user'
        }).then(function successCallback(response) {
            hideModal();
            
            if ( response.data != '') {
                $scope.plotNavigator.topPage.data = response.data;
                $scope.plotNavigator.topPage.data.daysLeft = '';
                if ( response.data.user.roles[0].slug == 'user' ) {
                    $scope.plotNavigator.topPage.data.daysLeft = response.data.daysLeft;
                }
            }
        },function errorCallback(response) {
            hideModal();
            ons.notification.alert(response.data);
        });

        navigator.geolocation.getCurrentPosition(function(position){
            var Latitude = position.coords.latitude;
            var Longitude = position.coords.longitude;
            
            $('.dashboard_map').gmap3({
                marker:{ 
                  latLng: new google.maps.LatLng(Latitude, Longitude)
                },
                map:{
                    options:{
                        zoom: 17,
                        mapTypeId: google.maps.MapTypeId.SATELLITE,
                        mapTypeControl: true,
                        mapTypeControlOptions: {
                        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                        },
                        navigationControl: true,
                        scrollwheel: true,
                        streetViewControl: false
                    }
                }
            });
        }, function(error){
            $('.dashboard_map').gmap3({
                map : {
                    options : {
                        center: [10.3124624, 123.8825883],
                        zoom:5,
                        mapTypeId: google.maps.MapTypeId.SATELLITE,
                        mapTypeControl: true,
                        mapTypeControlOptions: {
                            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                        },
                        navigationControl: true,
                        scrollwheel: true,
                        streetViewControl: false
                    }
                }
            });
        }, { 
            enableHighAccuracy: true 
        });
        
    };
    
    $scope.goTo = function(page)
    {
        plotNavigator.pushPage(page);
    }
});

module.controller('ViewAllPlotsController', function($scope, $http, $rootScope) {
    $scope.goTo = function(page)
    {
        plotNavigator.pushPage(page);
    }
});

module.controller('ViewAllPlotsTableViewController', function($scope, $http, $rootScope) {
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
        
    this.init = function(e) {
        showModal();
        
        $http({
            method: 'GET',
            url: BASEURL + '/auth/plots'
        }).then(function successCallback(response) {
            hideModal();
            if ( response.data != '') {
                $scope.plotNavigator.topPage.data.plots = response.data.plots;
            }
        },function errorCallback(response) {
            hideModal();
            ons.notification.alert(response.data);
        });
    };
    
    $scope.viewPlot = function(plotId) {
        viewPlotId = plotId;
        
        plotNavigator.pushPage('view_plot_info.html', {
            'animation': "slide",
            data: {
                'plot_id' : plotId
            }
        });
    };
    
    $scope.sharePlot = function(plotId) {
        viewPlotId = plotId;
        plotNavigator.pushPage('share_plot.html', {
            'animation': "slide",
            data: {
                'plot_id' : plotId
            }
        });
    };
    
    $scope.formatDate = function(date){
        var dateOut = new Date(date);
        return dateOut;
    };
    
    $scope.goTo = function(page)
    {
        plotNavigator.pushPage(page, {
            "animation": "slide"
        });
    };
});

module.controller('ViewAllPlotsMapViewController', function($scope, $timeout, $http, $rootScope) {
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
    
    this.init = function(e) {
        showModal();
    
        $http({
            method: 'GET',
            url: BASEURL + '/auth/plots/map-view'
        }).then(function successCallback(response) {
            hideModal();

            if ( response.data != '' ) {
                $scope.listCoordinates = JSON.parse(response.data.plots);
                $scope.LongLatArr = [];
                $scope.LngLatMarkers = [];

                $(".plots_map_view").gmap3({
                    map : {
                        options : {
                            center: [10.3124624, 123.8825883],
                            zoom:14,
                            mapTypeId: google.maps.MapTypeId.SATELLITE,
                            mapTypeControl: true,
                            mapTypeControlOptions: {
                                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                            },
                            navigationControl: true,
                            scrollwheel: true,
                            streetViewControl: false
                        }
                    }
                });

                for(var i = 0; i < $scope.listCoordinates.length; i++) {
                    if ( $scope.listCoordinates[i]['is_new_coordinates'] ) {
                        $scope.LongLatArr = JSON.parse($scope.listCoordinates[i]['coordinates']);
                        $.each($scope.LongLatArr, function(index, obj){
                            var point = index == 0 ? '': 'P'+ parseInt(index);
                            var LngLat = $scope.convert_to_latlng(obj[0], obj[1]);
                            var marker = 'data:image/svg+xml,<svg%20xmlns%3D"http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg"%20width%3D"38"%20height%3D"38"%20viewBox%3D"0%200%2038%2038"><text%20transform%3D"translate%2819%2018.5%29"%20fill%3D"%23FFFFFF"%20style%3D"font-family%3A%20Arial%2C%20sans-serif%3Bfont-weight%3Abold%3Btext-align%3Abottom%3B"%20font-size%3D"18"%20text-anchor%3D"middle">'+point+'<%2Ftext><%2Fsvg>';
                            var image = {
                                url: marker,
                                // This marker is 20 pixels wide by 32 pixels high.
                                size: new google.maps.Size(38, 38),
                                // The origin for this image is (0, 0).
                                origin: new google.maps.Point(0, 0),
                                // The anchor for this image is the base of the flagpole at (0, 32).
                                anchor: new google.maps.Point(20, 15)
                            };

                            $scope.LngLatMarkers.push({
                                latLng: obj,
                                options: {
                                    icon: image
                                }
                            });
                        });
                    } else {
                        $.each($scope.listCoordinates[i]['coordinates'], function(index, obj){
                            var LngLat = $scope.convert_to_latlng(obj[0], obj[1]);
                            var point = index == 0 ? '': 'P'+ parseInt(index);
                            var marker = 'data:image/svg+xml,<svg%20xmlns%3D"http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg"%20width%3D"38"%20height%3D"38"%20viewBox%3D"0%200%2038%2038"><text%20transform%3D"translate%2819%2018.5%29"%20fill%3D"%23FFFFFF"%20style%3D"font-family%3A%20Arial%2C%20sans-serif%3Bfont-weight%3Abold%3Btext-align%3Abottom%3B"%20font-size%3D"18"%20text-anchor%3D"middle">'+point+'<%2Ftext><%2Fsvg>';
                            var image = {
                                url: marker,
                                // This marker is 20 pixels wide by 32 pixels high.
                                size: new google.maps.Size(38, 38),
                                // The origin for this image is (0, 0).
                                origin: new google.maps.Point(0, 0),
                                // The anchor for this image is the base of the flagpole at (0, 32).
                                anchor: new google.maps.Point(20, 15)
                            };

                            $scope.LongLatArr.push( LngLat );
                            $scope.LngLatMarkers.push({
                                latLng: LngLat,
                                options: {
                                    icon: image
                                }
                            });
                        });
                    }

                    $(".plots_map_view").gmap3({
                        marker:{
                            values: $scope.LngLatMarkers
                        },
                        polygon : {
                            options : {
                                strokeColor: "yellow",
                                strokeOpacity: 0.8,
                                strokeWeight: 5,
                                fillColor: "yellow",
                                fillOpacity: 0.35,
                                paths: $scope.LongLatArr,
                                draggable: false
                            }
                        },
                        autofit: {}
                    });

                    $scope.LongLatArr = [];
                }
            }
        },function errorCallback(response) {
            hideModal();
            ons.notification.alert(response.data);
        });
    };
    
    $scope.formatDate = function(date){
        var dateOut = new Date(date);
        return dateOut;
    };
    
    $scope.goBack = function(page) {
        plotNavigator.pushPage(page, {
            "animation": "slide"
        });
    }
    
    $scope.goTo = function(page)
    {
        plotNavigator.pushPage(page, {
            "animation": "slide"
        });
    };
    
    $scope.convert_to_latlng = function(northing, easting){
        var result = [];
        var ll = $scope.UTMtoLL(northing, easting, 51);
        ll[0] = Math.round(ll[0]*1000000)/1000000;
        ll[1] = Math.round(ll[1]*1000000)/1000000;
        result.push(ll[1], ll[0]);

        return result;
    };

    $scope.UTMtoLL = function(f,f1,j) {
        var d = 0.99960000000000004;
        var d1 = 6378137;
        var d2 = 0.0066943799999999998;

        var d4 = (1 - Math.sqrt(1-d2))/(1 + Math.sqrt(1 - d2));
        var d15 = f1 - 500000;
        var d16 = f;
        var d11 = ((j - 1) * 6 - 180) + 3;

        var d3 = d2/(1 - d2);
        var d10 = d16 / d;
        var d12 = d10 / (d1 * (1 - d2/4 - (3 * d2 *d2)/64 - (5 * Math.pow(d2,3))/256));
        var d14 = d12 + ((3*d4)/2 - (27*Math.pow(d4,3))/32) * Math.sin(2*d12) + ((21*d4*d4)/16 - (55 * Math.pow(d4,4))/32) * Math.sin(4*d12) + ((151 * Math.pow(d4,3))/96) * Math.sin(6*d12);
        var d13 = d14 * 180 / Math.PI;
        var d5 = d1 / Math.sqrt(1 - d2 * Math.sin(d14) * Math.sin(d14));
        var d6 = Math.tan(d14)*Math.tan(d14);
        var d7 = d3 * Math.cos(d14) * Math.cos(d14);
        var d8 = (d1 * (1 - d2))/Math.pow(1-d2*Math.sin(d14)*Math.sin(d14),1.5);

        var d9 = d15/(d5 * d);
        var d17 = d14 - ((d5 * Math.tan(d14))/d8)*(((d9*d9)/2-(((5 + 3*d6 + 10*d7) - 4*d7*d7-9*d3)*Math.pow(d9,4))/24) + (((61 +90*d6 + 298*d7 + 45*d6*d6) - 252*d3 -3 * d7 *d7) * Math.pow(d9,6))/720); 
        d17 = d17 * 180 / Math.PI;
        var d18 = ((d9 - ((1 + 2 * d6 + d7) * Math.pow(d9,3))/6) + (((((5 - 2 * d7) + 28*d6) - 3 * d7 * d7) + 8 * d3 + 24 * d6 * d6) * Math.pow(d9,5))/120)/Math.cos(d14);
        d18 = d11 + d18 * 180 / Math.PI;
        return [d18,d17];
    };
});

module.controller('AddNewPlotController', function($scope, $http, $rootScope) {
    document.addEventListener('prechange', function(event) {
        document.querySelector('ons-toolbar .header-label').innerHTML = event.tabItem.getAttribute('label');
    });
    
    $scope.nextTab = function(index) {
        document.querySelector('ons-tabbar').setActiveTab(index);
    }
    
});

module.controller('ViewAllSharedPlotsController', function($scope, $http, $rootScope) {
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
        
    $http({
        method: 'GET',
        url: BASEURL + '/auth/shared-plots'
    }).then(function successCallback(response) {
        if ( response.data != '') {
            $scope.plotNavigator.topPage.data.plots = response.data.plots;
        }
    },function errorCallback(response) {
        ons.notification.alert(response.data);
    });
    
    $scope.formatDate = function(date){
        var dateOut = new Date(date);
        return dateOut;
    };
    
    $scope.viewPlot = function(plotId) {
        viewPlotId = plotId;
        
        plotNavigator.pushPage('view_plot_info.html', {
            'animation': "slide",
            data: {
                'plot_id' : plotId
            }
        });
    };
});

module.controller('PlotInfoController', function($scope, $http, $rootScope) {
    this.init = function(e) {
        // Ensure the emitter is the current page, not a nested one
        if (e.target === e.currentTarget) {
            $scope.plotInfo.type_of_plot = "single";
        }
    };
    
    $scope.next = function() {
        if ( $scope.plotInfo.name_of_plot != undefined && $scope.plotInfo.name_of_plot != '' ) {
            plotNavigator.pushPage('tech_description.html', {
                animation: 'slide',
                data: {
                    plot_name: $scope.plotInfo.name_of_plot,
                    plot_type: $scope.plotInfo.type_of_plot,
                }
            });
        } else {
            ons.notification.alert('Please Enter Plot Name.');
        }
    }
});

module.controller('TechDescriptionController', function($scope, $http, $rootScope) {
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
    
    this.init = function(e) {
        // Ensure the emitter is the current page, not a nested one
        if (e.target === e.currentTarget) {
            var page = e.target;
            // Safely access data
            if ( page.data != undefined ) {
                $scope.plotInfo = page.data;
                $scope.techdesc.corners = 4;
                plotNavigator.topPage.data.number_corners = 4;
                
                showModal();
                
                $http({
                    method: 'GET',
                    url: BASEURL + '/auth/plot/provinces'
                }).then(function successCallback(response) {
                    hideModal();
                    if ( response.data != '') {
                        $scope.plotNavigator.topPage.data.provinces = response.data;
                        
                        $scope.initInputMask();
                    }
                },function errorCallback(response) {
                    ons.notification.alert(response.data);
                });
            }
        }
    };
    
    this.province = '';
    this.editProvinceSelects = function() {
        if ( this.province != undefined && this.province != '' ) {
            showModal();
            $http({
                method: 'GET',
                url: BASEURL + '/auth/plot/city-municipality/' + this.province
            }).then(function successCallback(response) {
                hideModal();
                if ( response.data != '') {
                    $scope.plotNavigator.topPage.data.cadastral_name = '';
                    $scope.plotNavigator.topPage.data.point_of_reference = '';
                    $scope.plotNavigator.topPage.data.city_municipality = response.data;
                }
            },function errorCallback(response) {
                ons.notification.alert(response.data);
            });
        } else {
            ons.notification.alert('Please select correct Province');
        }
    };
    
    this.city_municipality = '';
    this.editCityMunicipalitySelects = function() {
        if ( this.city_municipality != undefined && this.city_municipality != '' ) {
            showModal();
            $http({
                method: 'GET',
                url: BASEURL + '/auth/plot/locations/'+this.province+'/'+this.city_municipality
            }).then(function successCallback(response) {
                hideModal();
                if ( response.data != '') {
                    $scope.plotNavigator.topPage.data.point_of_reference = '';
                    $scope.plotNavigator.topPage.data.cadastral_name = response.data;
                }
            },function errorCallback(response) {
                ons.notification.alert(response.data);
            });
        } else {
            ons.notification.alert('Please select correct City/Municipality');
        }
    };
    
    this.cadastral_name = '';
    this.editCadastralNameSelects = function() {
        if ( this.cadastral_name != undefined && this.cadastral_name != '' ) {
            showModal();
            $http({
                method: 'GET',
                url: BASEURL + '/auth/plot/point-of-references/'+this.province+'/'+this.city_municipality+'/'+this.cadastral_name
            }).then(function successCallback(response) {
                hideModal();
                if ( response.data != '') {
                    $scope.plotNavigator.topPage.data.point_of_reference = response.data;
                }
            },function errorCallback(response) {
                ons.notification.alert(response.data);
            });
        } else {
            ons.notification.alert('Please select correct Cadastral Name');
        }
    };
    
    this.editCornerPoints = function() {
        $scope.plotNavigator.topPage.data.number_corners = $scope.techdesc.corners;
        
        $scope.initInputMask();
    };
    
    $scope.next = function() {
        showModal();
        $http({
            method: 'POST',
            url: BASEURL + '/plot/plot-map',
            data: {
                'tech_desc' : $('#technical-descriptions').serialize()
            },
            header: {
                'Accept' : 'application/json',
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(function successCallback(response) {
            hideModal();
            if ( response.data != '') {
                plotNavigator.pushPage('plot_result.html', {
                    animation: 'slide',
                    data: {
                            plot_name: $scope.plotInfo.plot_name,
                            plot_type: $scope.plotInfo.plot_type,
                            coordinates: response.data.coordinates,
                            plot_id: response.data.plot_id,
                            reference_id: $scope.techdesc.point_of_reference,
                            technical_description: $('#technical-descriptions').serialize()
                        }
                });
            }
        },function errorCallback(response) {
            hideModal();
            ons.notification.alert(response.data);
        });
    };
    
    $scope.range = function(min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
        
        $scope.initInputMask();
        
        return input;
    };
    
    $scope.initInputMask = function()
    {
        //DMS
        $('.dms-input').inputmask({
            mask: ["X 99°99′ Y"],
            definitions: {
                "X": {
                  validator: "[nNsS]",
                  casing: "upper"
                },
                "Y": {
                  validator: "[eEwW]",
                  casing: "upper"
                }
            }
        });
    };
    
    $scope.back = function()
    {
        plotNavigator.pushPage('plot_info.html', {
            animation: 'slide'
        });
    };
});

module.controller('PlotResultController', function($scope, $http, $rootScope) {
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
    var polygonUniqueId = '';
    
    this.init = function(e)
    {
        if (e.target === e.currentTarget) {
            var page = e.target;
            // Safely access data
            if ( page.data != undefined ) {
                $scope.plotId = page.data.plot_id;
                $scope.coordinates = page.data.coordinates;
                $scope.plot_name = page.data.plot_name;
                $scope.plot_type = page.data.plot_type;
                $scope.technical_description = page.data.technical_description;
                $scope.LongLatArr = [];
                $scope.LngLatMarkers = [];
                $scope.newCoordinate = '';
                
                if ( $scope.plot_type == 'single' ) {
                    $('#plot_map').gmap3({
                        clear: {
                            name:["polygon"],
                            last: true
                        }
                    });
                }
                
                $.each($scope.coordinates, function(index, obj){
                    $scope.LngLat = $scope.convert_to_latlng(obj[0], obj[1]);
                    $scope.LongLatArr.push( $scope.LngLat );

                    var point = index == 0 ? '': 'P'+ parseInt(index);
                    var marker = 'data:image/svg+xml,<svg%20xmlns%3D"http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg"%20width%3D"38"%20height%3D"38"%20viewBox%3D"0%200%2038%2038"><text%20transform%3D"translate%2819%2018.5%29"%20fill%3D"%23FFFFFF"%20style%3D"font-family%3A%20Arial%2C%20sans-serif%3Bfont-weight%3Abold%3Btext-align%3Abottom%3B"%20font-size%3D"18"%20text-anchor%3D"middle">'+point+'<%2Ftext><%2Fsvg>';
                    var image = {
                        url: marker,
                        // This marker is 20 pixels wide by 32 pixels high.
                        size: new google.maps.Size(38, 38),
                        // The origin for this image is (0, 0).
                        origin: new google.maps.Point(0, 0),
                        // The anchor for this image is the base of the flagpole at (0, 32).
                        anchor: new google.maps.Point(20, 15)
                    };

                    $scope.LngLatMarkers.push({
                        latLng: $scope.LngLat,
                        options: {
                            icon: image
                        },
                        tag: 'marker'
                    });
                });
                
                $("#plot_map").gmap3({
                    map : {
                        options : {
                            center: [10.3124624, 123.8825883],
                            zoom:10,
                            mapTypeId: google.maps.MapTypeId.SATELLITE,
                            mapTypeControl: true,
                            mapTypeControlOptions: {
                                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                            },
                            navigationControl: true,
                            scrollwheel: true,
                            streetViewControl: false
                        }
                    },
                    marker:{
                        values: $scope.LngLatMarkers
                    },
                    polygon : {
                        options : {
                            center: [$scope.LongLatArr[0], $scope.LongLatArr[1]],
                            strokeColor: "yellow",
                            strokeOpacity: 0.8,
                            strokeWeight: 5,
                            fillColor: "yellow",
                            fillOpacity: 0.35,
                            paths: $scope.LongLatArr,
                            draggable: true
                        },
                        events: {
                            dragend: function(polygon){
                                newCoordinates = [];
                                var LngLatMarkers = [];

                                $("#plot_map").gmap3({
                                    clear: {
                                        tag: ['marker', polygonUniqueId]
                                    }
                                });

                                polygonUniqueId = Date.now();

                                for (var i = 0; i < polygon.getPath().getLength(); i++) {
                                    var xy = polygon.getPath().getAt(i);
                                    newCoordinates.push([xy.lat(), xy.lng()]);

                                    var point = i == 0 ? '': 'P'+ parseInt(i);
                                    var marker = 'data:image/svg+xml,<svg%20xmlns%3D"http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg"%20width%3D"38"%20height%3D"38"%20viewBox%3D"0%200%2038%2038"><text%20transform%3D"translate%2819%2018.5%29"%20fill%3D"%23FFFFFF"%20style%3D"font-family%3A%20Arial%2C%20sans-serif%3Bfont-weight%3Abold%3Btext-align%3Abottom%3B"%20font-size%3D"18"%20text-anchor%3D"middle">'+point+'<%2Ftext><%2Fsvg>';
                                    var image = {
                                        url: marker,
                                        // This marker is 20 pixels wide by 32 pixels high.
                                        size: new google.maps.Size(38, 38),
                                        // The origin for this image is (0, 0).
                                        origin: new google.maps.Point(0, 0),
                                        // The anchor for this image is the base of the flagpole at (0, 32).
                                        anchor: new google.maps.Point(20, 15)
                                    };

                                    LngLatMarkers.push({
                                        latLng: [xy.lat(), xy.lng()],
                                        options: {
                                            icon: image
                                        },
                                        tag: polygonUniqueId
                                    });
                                }

                                $("#plot_map").gmap3({
                                    marker:{
                                        values: LngLatMarkers
                                    }
                                });
                                
                                $scope.coordinates = JSON.stringify(newCoordinates);
                            }
                        }
                    },
                    autofit: {}
                });
            }
        }
    };
    
    $scope.back = function()
    {
        plotNavigator.pushPage('tech_description.html', {
            animation: 'slide'
        });
    };
    
    $scope.save_close = function()
    {
        showModal();
        
        $http({
            method: 'POST',
            url: BASEURL + '/plot/save-map',
            data: {
                'plot_info' : $scope.technical_description,
                'plot_id' : $scope.plotId,
                'new_coordinates' : $scope.coordinates,
                'plot_name' : $scope.plot_name,
                'plot_type' : $scope.plot_type,
                'user_id'   : userInfo.user.id
            },
            header: {
                'Accept' : 'application/json',
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(function successCallback(response) {
            hideModal();
            if ( response.data != '') {
                plotNavigator.pushPage('all_plots_table_view.html', {
                    animation: 'slide'
                });
            }
        },function errorCallback(response) {
            hideModal();
            ons.notification.alert(response.data);
        });
    };
    
    $scope.convert_to_latlng = function(northing, easting)
    {
        var result = [];
        var ll = $scope.UTMtoLL(northing, easting, 51);
        ll[0] = Math.round(ll[0]*1000000)/1000000;
        ll[1] = Math.round(ll[1]*1000000)/1000000;
        result.push(ll[1], ll[0]);

        return result;
    }
    
    $scope.UTMtoLL = function(f, f1, j)
    {
        var d = 0.99960000000000004;
        var d1 = 6378137;
        var d2 = 0.0066943799999999998;

        var d4 = (1 - Math.sqrt(1-d2))/(1 + Math.sqrt(1 - d2));
        var d15 = f1 - 500000;
        var d16 = f;
        var d11 = ((j - 1) * 6 - 180) + 3;

        var d3 = d2/(1 - d2);
        var d10 = d16 / d;
        var d12 = d10 / (d1 * (1 - d2/4 - (3 * d2 *d2)/64 - (5 * Math.pow(d2,3))/256));
        var d14 = d12 + ((3*d4)/2 - (27*Math.pow(d4,3))/32) * Math.sin(2*d12) + ((21*d4*d4)/16 - (55 * Math.pow(d4,4))/32) * Math.sin(4*d12) + ((151 * Math.pow(d4,3))/96) * Math.sin(6*d12);
        var d13 = d14 * 180 / Math.PI;
        var d5 = d1 / Math.sqrt(1 - d2 * Math.sin(d14) * Math.sin(d14));
        var d6 = Math.tan(d14)*Math.tan(d14);
        var d7 = d3 * Math.cos(d14) * Math.cos(d14);
        var d8 = (d1 * (1 - d2))/Math.pow(1-d2*Math.sin(d14)*Math.sin(d14),1.5);

        var d9 = d15/(d5 * d);
        var d17 = d14 - ((d5 * Math.tan(d14))/d8)*(((d9*d9)/2-(((5 + 3*d6 + 10*d7) - 4*d7*d7-9*d3)*Math.pow(d9,4))/24) + (((61 +90*d6 + 298*d7 + 45*d6*d6) - 252*d3 -3 * d7 *d7) * Math.pow(d9,6))/720); 
        d17 = d17 * 180 / Math.PI;
        var d18 = ((d9 - ((1 + 2 * d6 + d7) * Math.pow(d9,3))/6) + (((((5 - 2 * d7) + 28*d6) - 3 * d7 * d7) + 8 * d3 + 24 * d6 * d6) * Math.pow(d9,5))/120)/Math.cos(d14);
        d18 = d11 + d18 * 180 / Math.PI;
        return [d18,d17];
    }
});

module.controller('MyFriendsController', function($scope, $http, $rootScope) {
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
    
    this.init = function(e) {
        this.refreshFriendList();
    };
    
    this.refreshFriendList = function() {
        showModal();
        
        $http({
            method: 'GET',
            url: BASEURL + '/auth/plot/get-friends'
        }).then(function successCallback(response) {
            hideModal();
            
            $scope.plotNavigator.topPage.data.my_friends = response.data.friends;
        },function errorCallback(response) {
            hideModal();
            ons.notification.alert(response.data);
        });
    };
    
    this.delegate = {
        addNew: function(){
            ons.notification.prompt({
                message: 'Enter your friend email address',
                callback: function(email) {
                    if ( email == undefined || email == '') {
                        ons.notification.alert('Please enter email address');
                    } else {
                        showModal();
                        
                        $http({
                            method: 'GET',
                            url: BASEURL + '/auth/plot/add-friend/'+email
                        }).then(function successCallback(response) {
                            if ( response.data != '') {
                                if ( response.data.code ) {
                                    $http({
                                        method: 'GET',
                                        url: BASEURL + '/auth/plot/get-friends'
                                    }).then(function successCallback(result) {
                                        hideModal();
                                        ons.notification.alert(response.data.response);
                                        $scope.plotNavigator.topPage.data.my_friends = result.data.friends;
                                    },function errorCallback(response) {
                                        ons.notification.alert(response.data);
                                    });
                                } else {
                                    hideModal();
                                    ons.notification.alert(response.data.response);
                                }
                            }
                        },function errorCallback(response) {
                            hideModal();
                            ons.notification.alert(response.data);
                        });
                    }
                }
            });
        },
        remove: function(id) {
            ons.notification.confirm({
                title: "Remove",
                message: 'Are you sure you want to remove this friend?',
                buttonLabels: ["Yes", "Cancel"],
                callback: function(idx) {
                    switch(idx) {
                        case 0:
                                showModal();
                                
                                $http({
                                    method: 'GET',
                                    url: BASEURL + '/auth/plot/remove-friend/'+id
                                }).then(function successCallback(response) {
                                    if ( response.data != '') {
                                        if ( response.data.code ) {
                                            $http({
                                                method: 'GET',
                                                url: BASEURL + '/auth/plot/get-friends'
                                            }).then(function successCallback(result) {
                                                hideModal();
                                                ons.notification.alert(response.data.response);
                                                $scope.plotNavigator.topPage.data.my_friends = result.data.friends;
                                            },function errorCallback(response) {
                                                ons.notification.alert(response.data);
                                            });
                                        } else {
                                            hideModal();
                                            ons.notification.alert(response.data.response);
                                        }
                                    }
                                },function errorCallback(response) {
                                    hideModal();    
                                    ons.notification.alert(response.data);
                                });
                            break;
                        case 1:
                            break;
                    }
                }
            })
        }
    };
});

module.controller('ViewingPlotController', function($scope, $http, $rootScope) {
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
    
    this.init = function(e)
    {
        if (e.target === e.currentTarget) {
            var page = e.target;
            // Safely access data
            if ( page.data != undefined ) {
                
            }
        }
    };
    
    $scope.goBack = function()
    {
        plotNavigator.pushPage('all_plots_table_view.html', {
            animation: 'slide'
        });
    };
});

module.controller('ViewingPlotInfoController', function($scope, $http, $rootScope) {
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
    
    this.init = function(e)
    {   
        showModal();
                        
        $http({
            method: 'GET',
            url: BASEURL + '/auth/plot/info/'+viewPlotId
        }).then(function successCallback(response) {
            hideModal();
            if ( response.data != '') {
                $scope.viewing_plot_info.name_of_plot = response.data.plot_info.plot_name;
                $scope.viewing_plot_info.type_of_plot = response.data.plot_info.type;
            }
        },function errorCallback(response) {
            hideModal();
            ons.notification.alert(response.data);
        });
    }
});

module.controller('ViewingPlotTechDescController', function($scope, $http, $rootScope) {
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
    
    this.init = function(e)
    {   
        showModal();
                        
        $http({
            method: 'GET',
            url: BASEURL + '/auth/plot/info/'+viewPlotId
        }).then(function successCallback(response) {
            hideModal();
            if ( response.data != '') {
                $scope.viewingTechDesc.province = response.data.plot_info.point_reference.province;
                $scope.viewingTechDesc.city_municipality = response.data.plot_info.point_reference.municipality;
                $scope.viewingTechDesc.cadastral_name = response.data.plot_info.point_reference.location;
                $scope.viewingTechDesc.point_of_reference = response.data.plot_info.point_reference.point_of_reference;
                $scope.viewingTechDesc.number_corners = response.data.plot_info.num_corners;
                $scope.tech_description = JSON.parse(response.data.technical_description);
                $scope.viewingTechDesc.bearings = [];
                
                $.each($scope.tech_description, function(key, obj){
                    $scope.viewingTechDesc.bearings.push({
                        'bearing' : obj.bearing,
                        'distance' : obj.bearing_distance
                    });
                });
            }
        },function errorCallback(response) {
            hideModal();
            ons.notification.alert(response.data);
        });
    }
    
    $scope.range = function(min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
       
        return input;
    };
});

module.controller('ViewingPlotResult', function($scope, $http, $rootScope) {
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
    
    this.init = function(e)
    {   
        showModal();
                        
        $http({
            method: 'GET',
            url: BASEURL + '/auth/plot/info/'+viewPlotId
        }).then(function successCallback(response) {
            hideModal();
            if ( response.data != '') {
                $scope.LongLatArr = [];
                $scope.LngLatMarkers = [];
                $scope.newCoordinate = '';
                
                $(".viewing_plot_map").gmap3({
                        map : {
                            options : {
                                center: [10.3124624, 123.8825883],
                                zoom:10,
                                mapTypeId: google.maps.MapTypeId.SATELLITE,
                                mapTypeControl: true,
                                mapTypeControlOptions: {
                                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                                },
                                navigationControl: true,
                                scrollwheel: true,
                                streetViewControl: false
                            }
                        }
                });
                
                for(var i = 0; i < response.data.plot_coords.length; i++) {
                    if ( response.data.plot_coords[i]['is_new_coordinates'] ) {
                        $scope.LongLatArr = JSON.parse(response.data.plot_coords[i]['coordinates']);
                        $.each($scope.LongLatArr, function(index, obj){
                            var point = index == 0 ? '': 'P'+ parseInt(index);
                            var marker = 'data:image/svg+xml,<svg%20xmlns%3D"http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg"%20width%3D"38"%20height%3D"38"%20viewBox%3D"0%200%2038%2038"><text%20transform%3D"translate%2819%2018.5%29"%20fill%3D"%23FFFFFF"%20style%3D"font-family%3A%20Arial%2C%20sans-serif%3Bfont-weight%3Abold%3Btext-align%3Abottom%3B"%20font-size%3D"18"%20text-anchor%3D"middle">'+point+'<%2Ftext><%2Fsvg>';
                            var image = {
                                url: marker,
                                // This marker is 20 pixels wide by 32 pixels high.
                                size: new google.maps.Size(38, 38),
                                // The origin for this image is (0, 0).
                                origin: new google.maps.Point(0, 0),
                                // The anchor for this image is the base of the flagpole at (0, 32).
                                anchor: new google.maps.Point(20, 15)
                            };
                            $scope.LngLatMarkers.push({
                                latLng: obj,
                                options: {
                                    icon: image
                                }
                            });
                        });
                    } else {
                        $.each(response.data.plot_coords[i]['coordinates'], function(index, obj){
                            var point = index == 0 ? '': 'P'+ parseInt(index);
                            var LngLat = $scope.convert_to_latlng(obj[0], obj[1]);
                            var marker = 'data:image/svg+xml,<svg%20xmlns%3D"http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg"%20width%3D"38"%20height%3D"38"%20viewBox%3D"0%200%2038%2038"><text%20transform%3D"translate%2819%2018.5%29"%20fill%3D"%23FFFFFF"%20style%3D"font-family%3A%20Arial%2C%20sans-serif%3Bfont-weight%3Abold%3Btext-align%3Abottom%3B"%20font-size%3D"18"%20text-anchor%3D"middle">'+point+'<%2Ftext><%2Fsvg>';
                            var image = {
                                url: marker,
                                // This marker is 20 pixels wide by 32 pixels high.
                                size: new google.maps.Size(38, 38),
                                // The origin for this image is (0, 0).
                                origin: new google.maps.Point(0, 0),
                                // The anchor for this image is the base of the flagpole at (0, 32).
                                anchor: new google.maps.Point(20, 15)
                            };

                            $scope.LongLatArr.push( LngLat );
                            $scope.LngLatMarkers.push({
                                latLng: LngLat,
                                options: {
                                    icon: image
                                }
                            });
                        });
                    }
                    
                    $(".viewing_plot_map").gmap3({
                        marker:{
                            values: $scope.LngLatMarkers
                        },
                        polygon : {
                            options : {
                                strokeColor: "yellow",
                                strokeOpacity: 0.8,
                                strokeWeight: 5,
                                fillColor: "yellow",
                                fillOpacity: 0.35,
                                paths: $scope.LongLatArr,
                                draggable: false
                            }
                        },
                        autofit: {}
                    });
                }
            }
        },function errorCallback(response) {
            hideModal();
            ons.notification.alert(response.data);
        });
    };
    
    $scope.convert_to_latlng = function(northing, easting)
    {
        var result = [];
        var ll = $scope.UTMtoLL(northing, easting, 51);
        ll[0] = Math.round(ll[0]*1000000)/1000000;
        ll[1] = Math.round(ll[1]*1000000)/1000000;
        result.push(ll[1], ll[0]);

        return result;
    }
    
    $scope.UTMtoLL = function(f, f1, j)
    {
        var d = 0.99960000000000004;
        var d1 = 6378137;
        var d2 = 0.0066943799999999998;

        var d4 = (1 - Math.sqrt(1-d2))/(1 + Math.sqrt(1 - d2));
        var d15 = f1 - 500000;
        var d16 = f;
        var d11 = ((j - 1) * 6 - 180) + 3;

        var d3 = d2/(1 - d2);
        var d10 = d16 / d;
        var d12 = d10 / (d1 * (1 - d2/4 - (3 * d2 *d2)/64 - (5 * Math.pow(d2,3))/256));
        var d14 = d12 + ((3*d4)/2 - (27*Math.pow(d4,3))/32) * Math.sin(2*d12) + ((21*d4*d4)/16 - (55 * Math.pow(d4,4))/32) * Math.sin(4*d12) + ((151 * Math.pow(d4,3))/96) * Math.sin(6*d12);
        var d13 = d14 * 180 / Math.PI;
        var d5 = d1 / Math.sqrt(1 - d2 * Math.sin(d14) * Math.sin(d14));
        var d6 = Math.tan(d14)*Math.tan(d14);
        var d7 = d3 * Math.cos(d14) * Math.cos(d14);
        var d8 = (d1 * (1 - d2))/Math.pow(1-d2*Math.sin(d14)*Math.sin(d14),1.5);

        var d9 = d15/(d5 * d);
        var d17 = d14 - ((d5 * Math.tan(d14))/d8)*(((d9*d9)/2-(((5 + 3*d6 + 10*d7) - 4*d7*d7-9*d3)*Math.pow(d9,4))/24) + (((61 +90*d6 + 298*d7 + 45*d6*d6) - 252*d3 -3 * d7 *d7) * Math.pow(d9,6))/720); 
        d17 = d17 * 180 / Math.PI;
        var d18 = ((d9 - ((1 + 2 * d6 + d7) * Math.pow(d9,3))/6) + (((((5 - 2 * d7) + 28*d6) - 3 * d7 * d7) + 8 * d3 + 24 * d6 * d6) * Math.pow(d9,5))/120)/Math.cos(d14);
        d18 = d11 + d18 * 180 / Math.PI;
        return [d18,d17];
    }
});

module.controller('SharePlotController', function($scope, $http, $rootScope) {
    $http.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken;
    
    this.init = function(e) {
        this.refreshFriendList();
    };
    
    this.refreshFriendList = function() {
        showModal();
        
        $http({
            method: 'GET',
            url: BASEURL + '/auth/plot/get-friends'
        }).then(function successCallback(response) {
            hideModal();
            
            $scope.plotNavigator.topPage.data.my_friends = response.data.friends;
        },function errorCallback(response) {
            hideModal();
            ons.notification.alert(response.data);
        });
    };
    
    $scope.goBack = function(){
        plotNavigator.pushPage('all_plots_table_view.html', {
            animation: 'slide'
        });
    };
    
    this.delegate = {
        shareNow: function(){
            showModal();
        
            $http({
                method: 'POST',
                url: BASEURL + '/plot/share-plot',
                data: {
                    'plot_info' : $('#shareFriendsForm').serialize(),
                    'plot_id'   : viewPlotId,
                    'user_id'   : userInfo.user.id
                },
                header: {
                    'Accept' : 'application/json',
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(function successCallback(response) {
                hideModal();
                if ( response.data != '') {
                    ons.notification.alert({
                        message: response.data,
                        callback: function(){
                            plotNavigator.pushPage('all_plots_table_view.html', {
                                animation: 'slide'
                            });
                        }
                    });
                }
            },function errorCallback(response) {
                hideModal();
                ons.notification.alert(response.data);
            });
        }
    }
});



function showModal() {
    var modal = document.querySelector('ons-modal');
    modal.show();
}

function hideModal() {
    var modal = document.querySelector('ons-modal');
    modal.hide();
}